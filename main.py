import math

class ComplexNumber:
    def __init__(self, real_part, imaginary_part):
        self.real = real_part
        self.imaginary = imaginary_part

    def __str__(self):
        if self.imaginary >= 0:
            return f"{self.real} + {self.imaginary}i"
        else:
            return f"{self.real} - {abs(self.imaginary)}i"

    def __add__(self, other):
        real_part = self.real + other.real
        imaginary_part = self.imaginary + other.imaginary
        return ComplexNumber(real_part, imaginary_part)

    def __sub__(self, other):
        real_part = self.real - other.real
        imaginary_part = self.imaginary - other.imaginary
        return ComplexNumber(real_part, imaginary_part)

    def __mul__(self, other):
        real_part = (self.real * other.real) - (self.imaginary * other.imaginary)
        imaginary_part = (self.real * other.imaginary) + (self.imaginary * other.real)
        return ComplexNumber(real_part, imaginary_part)

    def __divmod__(self, other):
        denominator = (other.real ** 2) + (other.imaginary ** 2)
        real_part = ((self.real * other.real) + (self.imaginary * other.imaginary)) / denominator
        imaginary_part = ((self.imaginary * other.real) - (self.real * other.imaginary)) / denominator
        return ComplexNumber(real_part, imaginary_part)

    def modulus(self):
        return math.sqrt((self.real ** 2) + (self.imaginary ** 2))

    def power(self, exponent):
        modulus = self.modulus()
        argument = math.atan2(self.imaginary, self.real)
        new_modulus = modulus ** exponent
        new_argument = argument * exponent
        real_part = new_modulus * math.cos(new_argument)
        imaginary_part = new_modulus * math.sin(new_argument)
        return ComplexNumber(real_part, imaginary_part)

    def argument(self):
        return math.atan2(self.imaginary, self.real)

    def root(self, n):
        modulus = self.modulus()
        argument = self.argument()
        new_modulus = modulus ** (1 / n)
        root_arguments = [argument / n + 2 * math.pi * k / n for k in range(n)]
        roots = []
        for arg in root_arguments:
            real_part = new_modulus * math.cos(arg)
            imaginary_part = new_modulus * math.sin(arg)
            roots.append(ComplexNumber(real_part, imaginary_part))
        return roots

def check_integer(num):
    try:
        int(num)
        return True
    except ValueError:
        return False

def wtite_numbers():
    print("Введите первое число")
    real_part = input("Введите вещественную часть: ")
    while (not check_integer(real_part)):
        print("Введите число")
        real_part = input("Введите вещественную часть: ")
    imaginary_part = input("Введите мнимую часть: ")
    while (not check_integer(imaginary_part)):
        print("Введите число")
        imaginary_part = input("Введите мнимую часть: ")
    num1 = ComplexNumber(int(real_part), int(imaginary_part))
    print("Введите второе число")
    real_part = input("Введите вещественную часть: ")
    while (not check_integer(real_part)):
        print("Введите число")
        real_part = input("Введите вещественную часть: ")
    imaginary_part = input("Введите мнимую часть: ")
    while (not check_integer(real_part)):
        print("Введите число")
        imaginary_part = input("Введите мнимую часть: ")
    num2 = ComplexNumber(int(real_part), int(imaginary_part))
    return [num1, num2]

if __name__ == "__main__":
    menu = ""
    real_part = ""
    imaginary_part = ""
    numbers = []

    while(menu != "stop"):
        print("add: сумма")
        print("sub: разность")
        print("mul: умножение")
        print("div: деление")
        print("power: возведение в степень")
        print("root: корень")
        print("compare: сравнение")
        print("stop: выход")
        menu = input()

        if(menu == "add"):
            numbers = wtite_numbers()
            num1 = numbers[0]
            num2 = numbers[1]

            print(num1 + num2)
        elif(menu == "sub"):
            numbers = wtite_numbers()
            num1 = numbers[0]
            num2 = numbers[1]
            print(num1 - num2)
        elif(menu == "mul"):
            numbers = wtite_numbers()
            num1 = numbers[0]
            num2 = numbers[1]
            print(num1 * num2)
        elif(menu == "div"):
            numbers = wtite_numbers()
            num1 = numbers[0]
            num2 = numbers[1]
            print(divmod(num1, num2))
        elif(menu == "power"):
            print("Введите первое число")
            real_part = input("Введите вещественную часть: ")
            while (not check_integer(real_part)):
                print("Введите число")
                real_part = input("Введите вещественную часть: ")
            imaginary_part = input("Введите мнимую часть: ")
            while (not check_integer(imaginary_part)):
                print("Введите число")
                imaginary_part = input("Введите мнимую часть: ")
            num1 = ComplexNumber(int(real_part), int(imaginary_part))
            power = input("Введите степень: ")
            while (not check_integer(power)):
                print("Введите число")
                power = input("Введите степень: ")
            print(num1.power(int(power)))
        elif(menu == "root"):
            print("Введите первое число")
            real_part = input("Введите вещественную часть: ")
            while (not check_integer(real_part)):
                print("Введите число")
                real_part = input("Введите вещественную часть: ")
            imaginary_part = input("Введите мнимую часть: ")
            while (not check_integer(imaginary_part)):
                print("Введите число")
                imaginary_part = input("Введите мнимую часть: ")
            num1 = ComplexNumber(int(real_part), int(imaginary_part))
            root = input("Введите степень корня: ")
            while (not check_integer(root)):
                print("Введите число")
                root = input("Введите степень: ")
            result = num1.root(int(root))
            for i in result:
                print(i)
        elif(menu == "compare"):
            numbers = wtite_numbers()
            num1 = numbers[0]
            num2 = numbers[1]
            if num1.real == num2.real and num1.imaginary == num2.imaginary:
                print("Числа равны")
            else:
                print("Числа не равны")
        elif(menu == "stop"):
            break
        else:
            print("ошибка ввода")


